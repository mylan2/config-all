#!/bin/bash

# Declare >>
## Variable 
source_url="https://repo.zabbix.com/zabbix/6.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.4-1+ubuntu22.04_all.deb"
source_file="zabbix-release_6.4-1+ubuntu22.04_all.deb"
zabbix_services="-y zabbix-server-mysql zabbix-agent zabbix-apache-conf zabbix-sql-scripts zabbix-frontend-php"
mysql_server="-y mysql-server"
mysql_conf="/etc/mysql/mysql.conf.d/mysqld.cnf"
default_root_pass="password"
change_root_pass="ALTER USER 'root@localhost' IDENTIFIED BY '$default_root_pass';"
zabbix_dbname="zabbix"
zabbix_dbuser="zabbix"
zabbix_conf="/etc/zabbix/zabbix_server.conf"
zabbix_server_agent_processes="zabbix-server zabbix-agent apache2"
# Zabbix Agent
zabbix_services_agent="-y zabbix-agent"
zabbix_agent_conf="/etc/zabbix/zabbix_agentd.conf"
#  
cr=`echo $'\n.'`
cr=${cr%.}
# option
zabbix_install='[1]. Install Zabbix'
mongodb_install='[2]. Install MongoDB'

## Function

comfirm() {
      while true; do
            read -p "$1 [Y]es or [N]o): " reply
            case "${reply:-Y}" in
                  [Yy]* ) return 0;;
                  [Nn]* ) return 1;;
                  * ) echo "Error: Please answer yes or no !!!";;
            esac
      done
} 
#> Echo newline
echon() { 
      echo -e "$1\n"
}
#> End line
nechon() {
      echo "$cr$1$cr"
}
#> Get ethernet card
eth() {
      ip r | grep default | awk '/default/ {print $5}'
}
#> get IP addresss
ipar() {
      ip -4 -o addr show $(eth) | awk '{print $4}' | cut -d "/" -f 1
}
#> Install Zabbix Repository 
zabbix_install() { 
      echon "#1.1 Download source package"
      sudo wget $source_url
      echon "#1.2. Instal source package"
      sudo dpkg -i $source_file
      echon "#1.3. Update ..."
      sudo apt update
      echon "#1.4. Install Zabbix services"
      sudo apt install $1
}
#> Install mongo
install_mongodb() {
                  nechon "========= # Installing mongod =========" 
                  #Install mongoDB community Edition on Ubuntu 22.04 (jammy)
                  #1. Import the public key used by the package management system
                  nechon "#1. Import the public key used by the package management system"
                  if which gpg >/dev/null; then 
                        nechon "#1.1. Gnupg Installed"
                  else
                        nechon "#1.1. Installing gnupg"
                        sudo apt -y install gnupg
                  fi
                  nechon "1.2. Import the MongoDB public GPG Key"
                  curl -fsSL https://pgp.mongodb.com/server-6.0.asc | \
                  sudo gpg -o /usr/share/keyrings/mongodb-server-6.0.gpg \
                  --dearmor
                  #2. Create a list file for MongoDB.
                  nechon "#2. Create a list file for MongoDB"
                  echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-6.0.gpg ] \
                              https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/6.0 multiverse" | \
                        sudo tee /etc/apt/sources.list.d/mongodb-org-6.0.list
                  #3. Reload local package database
                  nechon "#3. Reload local package database"
                  sudo apt-get update
                  #4. Install the MongoDB packages.
                  nechon "#4. Install the MongoDB packages latest version"
                  sudo apt install -y mongodb-org
                  #5. Start MongoDB.
                  nechon "#5. Start MongoDB"
                  sudo systemctl daemon-reload
                  sudo systemctl start mongod.service
                  sudo systemctl enable mongod.service
                  #6. Để truy cập mongdb từ xa:
                  nechon "#6. Set IP for MongoDB"
                  #6.1 Get ethernet card physical name
                  nechon "#6.1. Get ethernet card physical name"
                  eth=$(ip r | grep default | awk '/default/ {print $5}')
                  nechon $eth
                  #6.2 Get Ip address
                  nechon "#6.2. Get Ip address"
                  ip=$(ip -4 -o addr show $eth | awk '{print $4}' | cut -d "/" -f 1)
                  nechon $ip
                  #6.3 Set Ip for MongoDB 
                  nechon "#6.3. Set IP for MongoDB"
                  sudo sed -i "s/bindIp:.*/bindIp: $ip/g" /etc/mongod.conf
                  sudo systemctl restart mongod.service
}
#> Set up replica Primary
setup_replica_primary() {
      if comfirm "Do you want to config Replica Set now ?"; then
            nechon "======== #7 Setup Replica Set is in progress ========"
            read -p "#7.1. Enter replSetname: " replsetname
            # Key file path
            fileKeyPath='/var/lib/mongodb/mongokey'
            [ -f "$fileKeyPath" ] && nechon $fileKeyPath || touch $fileKeyPath
            # Create key file 
            nechon "#7.2. Create key file"
            openssl rand -base64 756 > $fileKeyPath
            sudo chmod 400 $fileKeyPath
            sudo chown -R mongodb:mongodb $fileKeyPath
            # Copy key file to secondary server
            if comfirm "Copy KEY FILE to Secondary node server ? (Secondary must be running)"; then
            read -p "${cr}#7.3. Enter the number of Secondary server: " numberserver
            for (( i=1; i<=$numberserver; i++ ));
            do 
                  read -p "${cr}Enter IP Secondary Server $i: " ipsv
                  read -p "${cr}Enter Username Secondary Server $i: " usv 
                  sudo scp $fileKeyPath $usv@$ipsv:/home/$usv && nechon "Copy to Server successfully" || nechon "Copy to Server Fails"
            done
            fi
            # Edit /etc/mongod.conf
            nechon "#7.4. Config mongod ..."
            sudo sed -i "s|#security:|security:\n  keyFile: $fileKeyPath|g" /etc/mongod.conf
            sudo sed -i "s/#replication:/replication:\n  replSetName: '$replsetname'/g" /etc/mongod.conf
            sudo systemctl restart mongod.service
      else
            nechon "Okay, exit" 
      fi
}
#> Set up replica Secondary
setup_replica_secondary() {
      if comfirm "Do you want to config Replica Set now ?"; then
            nechon "======== # Setup Replica Set is in progress ========"
            read -p "#1. Enter replSetname: " replsetname
            # Key file path
            current_user=$(pwd)
            key_file_name=mongokey
            key_file_source=${current_user}/$key_file_name
            key_file_path='/var/lib/mongodb/'
            if [ -f "$key_file_source" ]; then
                  sudo chmod 777 $key_file_source 
                  sudo cp -n $key_file_source $key_file_path
                  sudo chmod 400 $key_file_path$key_file_name
                   sudo chown -R mongodb:mongodb $key_file_path$key_file_name
            else 
                  nechon "Error: Key File not found !!!"            
            fi
            # edit /etc/mongod.conf
            nechon "#2. Config mongod ..."
            sudo sed -i "s|#security:|security:\n  keyFile: $key_file_path$key_file_name|g" /etc/mongod.conf
            sudo sed -i "s/#replication:/replication:\n  replSetName: '$replsetname'/g" /etc/mongod.conf
            sudo systemctl restart mongod.service
      else
            nechon "Okay, exit" 
      fi
}

## End Function

# << End Declare

# Check run as root
if [ "$EUID" -ne 0 ]; then
      nechon "Error: This script must be run as root !!!"
      exit
fi




# ====================================== Option  ====================================== 
read -p "${cr}Please choose option to install: ${cr}$zabbix_install ${cr}$mongodb_install${cr}Your choice: "  option_install
case "${option_install}" in
    [1] ) 
    # Check Zabbix
    if which zabbix >/dev/null; then 
        nechon "||||||||||| ZABBIX IS ALREADY INSTALLED |||||||||||"
        exit
    fi
# 
    # Choose option 
    zabbix_server='[1]. Install Zabbix Server'
    zabbix_agent='[2]. Install Zabbix Agent'
    read -p "${cr}Please choose option to install: ${cr}$zabbix_server ${cr}$zabbix_agent${cr}Your choice: "  option
    case "${option}" in
        [1] ) 
        # ================== Option install Zabbix ====================
        ## Install Zabbix Server
        nechon "#1. Install Zabbix Server"
        zabbix_install "$zabbix_services"
        ## Create initial database
        if dpkg --get-selections | grep mysql-server; then 
                nechon "#2 MySql-server is installed"
        else 
                nechon "====== #2 Install MySql ======"
                nechon "#2.1. MySql installing in process"
                sudo apt install $mysql_server
                sudo systemctl start mysql.service
                sudo systemctl enable mysql.service
                nechon "#2.2. Set IP for MySql Server"
                nechon "Note: Card found: $(eth)"
                # Get Ip address
                nechon "Note: IP found: $(ipar)"
                sudo sed -i "s/bind-address.*/bind-address = $(ipar)/" $mysql_conf
                sudo systemctl restart mysql.service
        fi
        nechon "====== #3. Create Initial Zabbix Database ======"
        if comfirm "Do you want to change root password ? "; then
                read -sp "Enter your password: " password
                default_root_pass=$password
                sudo mysql -u root -e $change_root_pass
                sudo mysql -u root -e "FLUSH PRIVILEGES;"
                nechon $'\n'
        fi
        ##  
        nechon "#3.1. Create Database Zabbix"
        sudo mysql -u root -p$default_root_pass -e "create database $zabbix_dbname character set utf8mb4 collate utf8mb4_bin;"
        nechon "#3.2. Create User Zabbix"
        sudo mysql -u root -p$default_root_pass -e "create user $zabbix_dbuser@localhost identified by '$default_root_pass';"
        nechon "#3.3. grant all privileges for user zabbix"
        sudo mysql -u root -p$default_root_pass -e "grant all privileges on $zabbix_dbname.* to $zabbix_dbuser@localhost;"
        nechon "#3.4. Enable global log_bin_trust_function_creators"
        sudo mysql -u root -p$default_root_pass -e "set global log_bin_trust_function_creators = 1;"
        nechon "#3.5. Import initial schema and data"
        sudo zcat /usr/share/zabbix-sql-scripts/mysql/server.sql.gz | mysql --default-character-set=utf8mb4 -u$zabbix_dbuser -p$default_root_pass $zabbix_dbname
        ## Disable log_bin_trust_function_creators option after importing database schema.
        nechon "#2.6. Disable global log_bin_trust_function_creators"
        sudo mysql -u root -p$default_root_pass -e "set global log_bin_trust_function_creators = 0;"
        nechon "#2.7. Configure the database for Zabbix server"
        
        sudo sed -i "s/# DBPassword=/DBPassword=$default_root_pass/" $zabbix_conf
        sudo sed -i "s/DBName=.*/DBName=$zabbix_dbname/" $zabbix_conf
        sudo sed -i "s/DBUser=.*/DBUser=$zabbix_dbname/" $zabbix_conf 
        nechon "#3. Start Zabbix server and agent processes"
        if sudo ufw status | grep -qw active; then
        nechon "#3.1. Configure firewall"
                sudo ufw allow 10050/tcp
                sudo ufw allow 10051/tcp
                sudo ufw allow 80/tcp
                sudo ufw reload
        fi
        sudo systemctl restart $zabbix_server_agent_processes 
        sudo systemctl enable $zabbix_server_agent_processes
        ;;
        [2] ) 
        ## Install Zabbix Agent 
        zabbix_install "$zabbix_services_agent"
        ## 
        nechon "#1.5. Configure Zabbix agent to be able to communicate with the Zabbix server"
        read -p "Enter Zabbix Server IP: " ip_zabbix_server
        read -p "Enter Host Name for Zabbix Agent: " hostname_zabbix_agent
        ## Config /etc/zabbix/zabbix_agentd.conf
        sudo sed -i "s/Server=.*/Server=$ip_zabbix_server/" $zabbix_agent_conf
        sudo sed -i "s/ServerActive=.*/ServerActive=$ip_zabbix_server/" $zabbix_agent_conf
        sudo sed -i "s/Hostname=.*/Hostname=$hostname_zabbix_agent/" $zabbix_agent_conf
        nechon "#1.6. Start and enable Zabbix Agent service"
        systemctl restart zabbix-agent
        systemctl enable --now zabbix-agent
        systemctl status zabbix-agent
        ## Enable firewall
        if sudo ufw status | grep -qw active; then
                echon "#2.7. Configure firewall"
                ufw allow from $ip_zabbix_server to any port 10050 proto tcp comment "Allow Zabbix Server"
        fi
        ;;
            * ) echo "Error: Please answer 1 or 2 !!!";;
    esac
    # ================== End option install Zabbix ======================
    ;;
    [2] )
    # =================== Option install MongoDB ========================
    ## Choose option 
    primary_node='[1]. Install Mongodb primary node'
    secondary_node='[2]. Install Mongodb secondary node'
    read -p "${cr}Please choose option to install: ${cr}$primary_node ${cr}$secondary_node${cr}Your choice: "  option
        case "${option}" in
            [1] )
# ============================ Install Mongodb Primary node ==============================
            nechon "====== Install Mongodb primary node ======"
            ## Check mongodb
            if which mongod >/dev/null
            then 
                nechon "|||||||||||| MONGODB IS ALREADY INSTALLED |||||||||||"
                ## Set up Replica Primary
                setup_replica_primary
            else
                ## Install mongodb
                install_mongodb
                ## 6.4 Create account MongoDB Admin 
                if comfirm "Create Admin MongoDB Account now? "
                then
                        nechon "#6.4. Create account MongoDB Admin"
                        read -p "${cr}Enter username: " username
                        read -sp "${cr}Enter password: " password
                        nechon
                ##  
                sudo mongosh --host $ip <<EOF
                use admin
                db.createUser(
                        {          
                            user: "$username",
                            pwd: "$password",
                            roles: [ { role: 'root', db: 'admin' } ]
                        }
                );
EOF
                nechon
                else exit
                fi
                ## Set up replica Primary
                setup_replica_primary      
            fi ;;
            [2] )
            # ================= Install Mongodb Secondary node ====================
            nechon "====== Install Mongodb Secondary node ======" 
            ## Check mongodb
            if which mongod >/dev/null
            then 
                nechon "||||||||||| MONGODB IS ALREADY INSTALLED |||||||||||"
                ## Set up Replica Secondary
                setup_replica_secondary
            else 
                ## Install mongodb
                install_mongodb
                ## Set up Replica Secondary
                setup_replica_secondary
            fi ;;
            * ) echo "Error: Please answer 1 or 2 !!!";;
        esac
    ## Restart mongodb service
    sudo systemctl restart mongod.service
    if  sudo systemctl status mongod | grep -qw active
    then
        echo "|||||||||||||||| MONGODB IS RUNNING |||||||||||||||||"
        echo -e "||||||||||| HAPPYYY 凸(¬‿¬)凸 SUCCESSFULL ||||||||||| \n"
    else
        nechon "MongoDB is starting...\n"
        sudo systemctl start mongod.service
        systemctl status mongod.service
    fi
    # ============================ End Option install MongoDB ==============================
    ;;
    * ) echo "Error: Please answer 1 or 2 !!!";;
esac

# 


